Categories:Phone & SMS
License:Fair License
Web Site:http://purl.org/net/smsfilter
Source Code:http://purl.org/net/smsfilter
Issue Tracker:http://purl.org/net/smsfilter
Donate:http://purl.org/net/smsfilter/donate
FlattrID:1109454

Auto Name:SMS Filter
Summary:Filter text messages by keyword or address
Description:
SMS Filter can filter incoming SMS messages before they reach your inbox.
You can filter by address, and optionally add parts that must occur in the
message for it to be blocked.

By default, SMS Filter will not delete incoming messages. You will get a
silent status bar notification when SMS Filter has filtered an incoming
message, which you can view by opening the notification or by opening the
application.

You can also tell SMS Filter to delete incoming messages, in which case it
will happily do so for you. To do this, just turn off the 'Save messages'
preference.
.

Repo Type:git
Repo:https://gitorious.org/sms-filter/sms-filter.git

Build:1.0,1
    commit=bd53e27d8571d025cb51241761ce480359ec1153
    target=android-8

Build:1.1,2
    commit=c44a961eaa0fb39ff0028212fb2794b16c4ee1e3
    target=android-8

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1
Current Version Code:2

