Categories:Games
License:GPLv3
Web Site:https://code.google.com/p/diedroid
Source Code:https://code.google.com/p/diedroid/source
Issue Tracker:https://code.google.com/p/diedroid/issues

Auto Name:DieDroid
Summary:Dice roller
Description:
This is an Android application for a dice roller suitable for many different purposes, but was
designed as a tool for pen-and-paper RPGs. Includes a general dice roller, a dice pool roller that
counts successes, and an ability score roller to roll stats for characters.
.

#Repo Type:hg
Repo Type:git
#Repo:https://code.google.com/p/diedroid
Repo:https://github.com/logomancer/diedroid

#Build Version:1.3.0,8,297ea5b61,target=android-10
Build:1.4.0,9
    commit=1.4.0
    srclibs=ActionBarSherlock@4.2.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.4.0
Current Version Code:9

