Categories:Office,Science & Education
License:ISC

Web Site:http://www.gringene.org/pastimes.html
Source Code:https://github.com/gringer/ConcentricClock
Issue Tracker:https://github.com/gringer/ConcentricClock/issues

Summary:Tell the time using concentric arcs
Description:
This is a clock that uses spinning and growing concentric arcs to tell
the time, and demonstrates in a dynamic way the factors of 60. The
minute hand is suggested by the start and end positions of the arcs,
while the hour of day is shown by the size of the outer arc.

Other apps by this developer: [[org.gringene.colourclock]]
.

Repo Type:git
Repo:https://github.com/gringer/ConcentricClock.git

Build:1.1,2
     commit=edf5d2cc394ead0be312973a20b043d011376705
     gradle=yes
     subdir=app

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1
Current Version Code:2
